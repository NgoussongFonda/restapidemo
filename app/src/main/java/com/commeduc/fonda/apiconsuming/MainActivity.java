package com.commeduc.fonda.apiconsuming;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.commeduc.fonda.apiconsuming.model.User;
import com.commeduc.fonda.apiconsuming.remote.APIUtils;
import com.commeduc.fonda.apiconsuming.remote.UserService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button btnGetUsersList;
    ListView listView;

    UserService userService;
    List<User> list = new ArrayList<>();

    String sortfield = "t.rowid";
    String sortorder = "ASC";
    String limit = "100";
    String DOLAPIKEY = "X2hZ5DbHWRf8qZiWtM304Gxbzp315T1h";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Retrofit 2 CRUD Demo");

        btnGetUsersList = (Button) findViewById(R.id.btnGetUsersList);
        listView = (ListView) findViewById(R.id.listView);
        userService = APIUtils.getUserService();

        btnGetUsersList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUsersList(sortfield, sortorder, limit);
                //get users list
                System.out.println(">>>>>>>>>>> before API");
                getUsersList(sortfield, sortorder, limit);
                System.out.println(">>>>>>>>>>> after API");
            }
        });

    }

    public void getUsersList(String sortfield, String sortorder, String limit){
        System.out.println(">>>>>>>>>>> Inside API");
        Call<List<User>> call = userService.getUsers(sortfield, sortorder, limit);
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if(response.isSuccessful()){
                    list = response.body();
                    System.out.println(">>>>>>>>>>>" +list.get(0).getSociete());
                    listView.setAdapter(new UserAdapter(MainActivity.this, R.layout.list_user, list));
                }
                else {
                    Toast.makeText(MainActivity.this, "Failed"+ call.request().url().toString(), Toast.LENGTH_SHORT).show();
                    System.out.println(">>>>>>>>>>> Failed" +call.request().url().toString());
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
                System.out.println(">>>>>>>>>>> Failed" +call.request().url().toString());
            }
        });
    }
}
