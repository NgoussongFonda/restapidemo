package com.commeduc.fonda.apiconsuming.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by fonda on 12/18/20.
 */

public class User implements Serializable {
    @SerializedName("mesgs")
    @Expose
    private String mesgs;

    @SerializedName("login")
    @Expose
    private String login;

    @SerializedName("pass")
    @Expose
    private String pass;

    @SerializedName("pass_indatabase")
    @Expose
    private String pass_indatabase;

    @SerializedName("pass_indatabase_crypted")
    @Expose
    private String pass_indatabase_crypted;

    @SerializedName("societe")
    @Expose
    private String societe;

    @SerializedName("company")
    @Expose
    private String company;

    @SerializedName("fk_soc")
    @Expose
    private int fk_soc;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("zip")
    @Expose
    private String zip;

    @SerializedName("town")
    @Expose
    private String town;

    @SerializedName("state_id")
    @Expose
    private int state_id;

    @SerializedName("state_code")
    @Expose
    private String state_code;

    @SerializedName("state")
    @Expose
    private String state;

    @SerializedName("email")
    @Expose
    private String email;
/*
    @SerializedName("socialnetworks")
    @Expose
    private String socialnetworks;
*/
    @SerializedName("skype")
    @Expose
    private String skype;

    @SerializedName("twitter")
    @Expose
    private String twitter;

    @SerializedName("facebook")
    @Expose
    private String facebook;

    @SerializedName("linkedin")
    @Expose
    private String linkedin;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("phone_perso")
    @Expose
    private String phone_perso;

    @SerializedName("phone_mobile")
    @Expose
    private String phone_mobile;

    @SerializedName("fax")
    @Expose
    private String fax;

    @SerializedName("poste")
    @Expose
    private String poste;

    @SerializedName("morphy")
    @Expose
    private String morphy;

    @SerializedName("public")
    @Expose
    private String publics;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("datec")
    @Expose
    private String datec;

    @SerializedName("datem")
    @Expose
    private String datem;

    @SerializedName("datevalid")
    @Expose
    private String datevalid;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("birth")
    @Expose
    private String birth;

    @SerializedName("typeid")
    @Expose
    private String typeid;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("need_subscription")
    @Expose
    private String need_subscription;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("user_login")
    @Expose
    private String user_login;

    @SerializedName("datefin")
    @Expose
    private String datefin;

    @SerializedName("first_subscription_date")
    @Expose
    private String first_subscription_date;

    @SerializedName("first_subscription_amount")
    @Expose
    private String first_subscription_amount;

    @SerializedName("last_subscription_date")
    @Expose
    private String last_subscription_date;

    @SerializedName("last_subscription_date_start")
    @Expose
    private String last_subscription_date_start;

    @SerializedName("last_subscription_date_end")
    @Expose
    private String last_subscription_date_end;

    @SerializedName("last_subscription_amount")
    @Expose
    private String last_subscription_amount;

    @SerializedName("entity")
    @Expose
    private String entity;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("import_key")
    @Expose
    private String import_key;
/*
    @SerializedName("array_options")
    @Expose
    private String array_options;
*/
    @SerializedName("array_languages")
    @Expose
    private String array_languages;

    @SerializedName("linkedObjectsIds")
    @Expose
    private String linkedObjectsIds;

    @SerializedName("canvas")
    @Expose
    private String canvas;

    @SerializedName("fk_project")
    @Expose
    private String fk_project;

    @SerializedName("contact")
    @Expose
    private String contact;

    @SerializedName("contact_id")
    @Expose
    private String contact_id;

    @SerializedName("thirdparty")
    @Expose
    private String thirdparty;

    @SerializedName("user")
    @Expose
    private String user;

    @SerializedName("origin")
    @Expose
    private String origin;

    @SerializedName("origin_id")
    @Expose
    private String origin_id;

    @SerializedName("ref")
    @Expose
    private String ref;

    @SerializedName("ref_ext")
    @Expose
    private String ref_ext;

    @SerializedName("statut")
    @Expose
    private String statut;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("country_id")
    @Expose
    private String country_id;

    @SerializedName("country_code")
    @Expose
    private String country_code;

    @SerializedName("barcode_type")
    @Expose
    private String barcode_type;

    @SerializedName("barcode_type_code")
    @Expose
    private String barcode_type_code;

    @SerializedName("barcode_type_label")
    @Expose
    private String barcode_type_label;

    @SerializedName("barcode_type_coder")
    @Expose
    private String barcode_type_coder;

    @SerializedName("mode_reglement_id")
    @Expose
    private String mode_reglement_id;

    @SerializedName("cond_reglement_id")
    @Expose
    private String cond_reglement_id;

    @SerializedName("cond_reglement")
    @Expose
    private String cond_reglement;

    @SerializedName("modelpdf")
    @Expose
    private String modelpdf;

    @SerializedName("last_main_doc")
    @Expose
    private String last_main_doc;

    @SerializedName("fk_account")
    @Expose
    private String fk_account;

    @SerializedName("note_public")
    @Expose
    private String note_public;

    @SerializedName("note_private")
    @Expose
    private String note_private;

    @SerializedName("note")
    @Expose
    private String note;

    @SerializedName("lines")
    @Expose
    private String lines;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("lastname")
    @Expose
    private String lastname;

    @SerializedName("firstname")
    @Expose
    private String firstname;

    @SerializedName("civility_id")
    @Expose
    private String civility_id;

    @SerializedName("date_creation")
    @Expose
    private String date_creation;

    @SerializedName("date_validation")
    @Expose
    private String date_validation;

    @SerializedName("date_modification")
    @Expose
    private String date_modification;

    @SerializedName("civility_code")
    @Expose
    private String civility_code;

    @SerializedName("civility")
    @Expose
    private String civility;

    @SerializedName("socid")
    @Expose
    private String socid;

    @SerializedName("model_pdf")
    @Expose
    private String model_pdf;

    @SerializedName("first_subscription_date_start")
    @Expose
    private String first_subscription_date_start;

    @SerializedName("first_subscription_date_end")
    @Expose
    private String first_subscription_date_end;

    public User() {

    }

    public String getMesgs() {
        return mesgs;
    }

    public void setMesgs(String mesgs) {
        this.mesgs = mesgs;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPass_indatabase() {
        return pass_indatabase;
    }

    public void setPass_indatabase(String pass_indatabase) {
        this.pass_indatabase = pass_indatabase;
    }

    public String getPass_indatabase_crypted() {
        return pass_indatabase_crypted;
    }

    public void setPass_indatabase_crypted(String pass_indatabase_crypted) {
        this.pass_indatabase_crypted = pass_indatabase_crypted;
    }

    public String getSociete() {
        return societe;
    }

    public void setSociete(String societe) {
        this.societe = societe;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getFk_soc() {
        return fk_soc;
    }

    public void setFk_soc(int fk_soc) {
        this.fk_soc = fk_soc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
/*
    public String getSocialnetworks() {
        return socialnetworks;
    }

    public void setSocialnetworks(String socialnetworks) {
        this.socialnetworks = socialnetworks;
    }
*/
    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone_perso() {
        return phone_perso;
    }

    public void setPhone_perso(String phone_perso) {
        this.phone_perso = phone_perso;
    }

    public String getPhone_mobile() {
        return phone_mobile;
    }

    public void setPhone_mobile(String phone_mobile) {
        this.phone_mobile = phone_mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getMorphy() {
        return morphy;
    }

    public void setMorphy(String morphy) {
        this.morphy = morphy;
    }

    public String getPublics() {
        return publics;
    }

    public void setPublics(String publics) {
        this.publics = publics;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDatec() {
        return datec;
    }

    public void setDatec(String datec) {
        this.datec = datec;
    }

    public String getDatem() {
        return datem;
    }

    public void setDatem(String datem) {
        this.datem = datem;
    }

    public String getDatevalid() {
        return datevalid;
    }

    public void setDatevalid(String datevalid) {
        this.datevalid = datevalid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNeed_subscription() {
        return need_subscription;
    }

    public void setNeed_subscription(String need_subscription) {
        this.need_subscription = need_subscription;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getDatefin() {
        return datefin;
    }

    public void setDatefin(String datefin) {
        this.datefin = datefin;
    }

    public String getFirst_subscription_date() {
        return first_subscription_date;
    }

    public void setFirst_subscription_date(String first_subscription_date) {
        this.first_subscription_date = first_subscription_date;
    }

    public String getFirst_subscription_amount() {
        return first_subscription_amount;
    }

    public void setFirst_subscription_amount(String first_subscription_amount) {
        this.first_subscription_amount = first_subscription_amount;
    }

    public String getLast_subscription_date() {
        return last_subscription_date;
    }

    public void setLast_subscription_date(String last_subscription_date) {
        this.last_subscription_date = last_subscription_date;
    }

    public String getLast_subscription_date_start() {
        return last_subscription_date_start;
    }

    public void setLast_subscription_date_start(String last_subscription_date_start) {
        this.last_subscription_date_start = last_subscription_date_start;
    }

    public String getLast_subscription_date_end() {
        return last_subscription_date_end;
    }

    public void setLast_subscription_date_end(String last_subscription_date_end) {
        this.last_subscription_date_end = last_subscription_date_end;
    }

    public String getLast_subscription_amount() {
        return last_subscription_amount;
    }

    public void setLast_subscription_amount(String last_subscription_amount) {
        this.last_subscription_amount = last_subscription_amount;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImport_key() {
        return import_key;
    }

    public void setImport_key(String import_key) {
        this.import_key = import_key;
    }
/*
    public String getArray_options() {
        return array_options;
    }

    public void setArray_options(String array_options) {
        this.array_options = array_options;
    }
*/
    public String getArray_languages() {
        return array_languages;
    }

    public void setArray_languages(String array_languages) {
        this.array_languages = array_languages;
    }

    public String getLinkedObjectsIds() {
        return linkedObjectsIds;
    }

    public void setLinkedObjectsIds(String linkedObjectsIds) {
        this.linkedObjectsIds = linkedObjectsIds;
    }

    public String getCanvas() {
        return canvas;
    }

    public void setCanvas(String canvas) {
        this.canvas = canvas;
    }

    public String getFk_project() {
        return fk_project;
    }

    public void setFk_project(String fk_project) {
        this.fk_project = fk_project;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public String getThirdparty() {
        return thirdparty;
    }

    public void setThirdparty(String thirdparty) {
        this.thirdparty = thirdparty;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(String origin_id) {
        this.origin_id = origin_id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getRef_ext() {
        return ref_ext;
    }

    public void setRef_ext(String ref_ext) {
        this.ref_ext = ref_ext;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getBarcode_type() {
        return barcode_type;
    }

    public void setBarcode_type(String barcode_type) {
        this.barcode_type = barcode_type;
    }

    public String getBarcode_type_code() {
        return barcode_type_code;
    }

    public void setBarcode_type_code(String barcode_type_code) {
        this.barcode_type_code = barcode_type_code;
    }

    public String getBarcode_type_label() {
        return barcode_type_label;
    }

    public void setBarcode_type_label(String barcode_type_label) {
        this.barcode_type_label = barcode_type_label;
    }

    public String getBarcode_type_coder() {
        return barcode_type_coder;
    }

    public void setBarcode_type_coder(String barcode_type_coder) {
        this.barcode_type_coder = barcode_type_coder;
    }

    public String getMode_reglement_id() {
        return mode_reglement_id;
    }

    public void setMode_reglement_id(String mode_reglement_id) {
        this.mode_reglement_id = mode_reglement_id;
    }

    public String getCond_reglement_id() {
        return cond_reglement_id;
    }

    public void setCond_reglement_id(String cond_reglement_id) {
        this.cond_reglement_id = cond_reglement_id;
    }

    public String getCond_reglement() {
        return cond_reglement;
    }

    public void setCond_reglement(String cond_reglement) {
        this.cond_reglement = cond_reglement;
    }

    public String getModelpdf() {
        return modelpdf;
    }

    public void setModelpdf(String modelpdf) {
        this.modelpdf = modelpdf;
    }

    public String getLast_main_doc() {
        return last_main_doc;
    }

    public void setLast_main_doc(String last_main_doc) {
        this.last_main_doc = last_main_doc;
    }

    public String getFk_account() {
        return fk_account;
    }

    public void setFk_account(String fk_account) {
        this.fk_account = fk_account;
    }

    public String getNote_public() {
        return note_public;
    }

    public void setNote_public(String note_public) {
        this.note_public = note_public;
    }

    public String getNote_private() {
        return note_private;
    }

    public void setNote_private(String note_private) {
        this.note_private = note_private;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getLines() {
        return lines;
    }

    public void setLines(String lines) {
        this.lines = lines;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getCivility_id() {
        return civility_id;
    }

    public void setCivility_id(String civility_id) {
        this.civility_id = civility_id;
    }

    public String getDate_creation() {
        return date_creation;
    }

    public void setDate_creation(String date_creation) {
        this.date_creation = date_creation;
    }

    public String getDate_validation() {
        return date_validation;
    }

    public void setDate_validation(String date_validation) {
        this.date_validation = date_validation;
    }

    public String getDate_modification() {
        return date_modification;
    }

    public void setDate_modification(String date_modification) {
        this.date_modification = date_modification;
    }

    public String getCivility_code() {
        return civility_code;
    }

    public void setCivility_code(String civility_code) {
        this.civility_code = civility_code;
    }

    public String getCivility() {
        return civility;
    }

    public void setCivility(String civility) {
        this.civility = civility;
    }

    public String getSocid() {
        return socid;
    }

    public void setSocid(String socid) {
        this.socid = socid;
    }

    public String getModel_pdf() {
        return model_pdf;
    }

    public void setModel_pdf(String model_pdf) {
        this.model_pdf = model_pdf;
    }

    public String getFirst_subscription_date_start() {
        return first_subscription_date_start;
    }

    public void setFirst_subscription_date_start(String first_subscription_date_start) {
        this.first_subscription_date_start = first_subscription_date_start;
    }

    public String getFirst_subscription_date_end() {
        return first_subscription_date_end;
    }

    public void setFirst_subscription_date_end(String first_subscription_date_end) {
        this.first_subscription_date_end = first_subscription_date_end;
    }
}

