package com.commeduc.fonda.apiconsuming.remote;

/**
 * Created by fonda on 12/18/20.
 */

public class APIUtils {

    private APIUtils(){
    };

    public static final String API_URL = "https://helep.commeduc.com/api/index.php/";

    public static UserService getUserService(){
        return RetrofitClient.getClient(API_URL).create(UserService.class);
    }
}