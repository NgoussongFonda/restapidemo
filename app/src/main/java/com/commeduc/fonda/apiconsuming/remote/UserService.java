package com.commeduc.fonda.apiconsuming.remote;

import com.commeduc.fonda.apiconsuming.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by fonda on 12/18/20.
 */

public interface UserService {
    @Headers({
            "Accept: application/json",
            "DOLAPIKEY: X2hZ5DbHWRf8qZiWtM304Gxbzp315T1h"
    })
    @GET("members")
    Call<List<User>> getUsers(
            @Query(value = "sortfield") String sortfield,
            @Query(value = "sortorder") String sortorder,
            @Query(value = "limit") String limit
    );

}

/*
@Headers({
        "Accept: application/json",
        "DOLAPIKEY: X2hZ5DbHWRf8qZiWtM304Gxbzp315T1h"
})
*/